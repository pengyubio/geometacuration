GEOMetaCuration is developed to facilitate the manual curation of metadata on GEO.
See the [preprint manuscript](https://www.biorxiv.org/content/early/2018/01/31/257444) in bioRxiv.
## 1. Deploying GEOMetaCuration to a new server(Ubuntu 14.04).
#### 1.1 Download source code with Git.
```
git clone https://zhaoli2017@bitbucket.org/yubiolab/geometacuration.git
```
If you don’t have a git tool installed on the machine, please see ref[1] to install it.

#### 1.2 Install Python(2.7.13)
```
sudo apt-get update
sudo apt-get install build-essential checkinstall
sudo apt-get install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev
sudo wget https://www.python.org/ftp/python/2.7.13/Python-2.7.13.tgz
sudo tar xzf Python-2.7.13.tgz
cd Python-2.7.13
sudo ./configure
sudo make altinstall
```

#### 1.3 Install Pip
```
sudo apt install python-pip
```

#### 1.4 Install Django 1.11.6
```
pip install Django==1.11.6
```

#### 1.5 Install linux brew
```
sudo apt install linuxbrew-wrapper
```

#### 1.6 Install mysql server
```
sudo apt-get update
sudo apt-get install mysql-server
```

#### 1.7 Set-up MySQL for Python
```
apt-get install python-dev libmysqlclient-dev
pip install MySQL-python
```

#### 1.8 Install required packages for Python
```
pip install bs4 selenium requests html5lib
```

#### 1.9 Install phantomjs
```
wget https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2
tar xvfj phantomjs-2.1.1-linux-x86_64.tar.bz2
```
Add ```phantomjs-2.1.1-linux-x86_64/bin``` to path

#### 1.10 Create initial DJANGO migrations
```
python manage.py migrate --fake
python manage.py makemigrations admincuration
python manage.py migrate --fake-initial
```

## 2 other tools in GEOMetaCuration
#### 2.1 import local datasets to database
The tool is at ```GEOMetaCuration/tools/database/import_dataset/insert.sh```.
Run the following command to import test datasets:
```
database_user=root   ##put your database user here
database_user=password  ## put your database password here
./insert.sh $database_user $database_psd ./test.txt
```

#### 2.2 export curation result to file
This tool is at ```GEOMetaCuration/tools/database/dnld_result/export.sh```.
Run the following command to export curation result to a file:
```
database_user=root   ##put your database user here
database_user=password  ## put your database password here
./export.sh $database_user $database_psd > ./test.txt
```

#### 2.3 Retrieve initial metadata list from GEO
This tools is at ```GEOMetaCuration/tools/dnldfromGEO/```.
Run the following command to get the demo initial dataset list from GEO.
```
./main.sh
```
To curate other topics, replace the keyword in ```main.sh```.



