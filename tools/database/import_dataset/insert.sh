#!/usr/bin/env bash
# vim: set noexpandtab tabstop=2:

set -v

user=$1
psd=$2
file=$3

IFS=$'\t'; while read -r title accNo pubNo highlight keywords topic
do
	echo $title $accNo $pubNo $highlight $keywords $topic
	sql="INSERT INTO curation_proj.admincuration_dataset (title, accNo, pubNo, highlight, keywords, topic) VALUES (\"$title\", \"$accNo\", \"$pubNo\", \"$highlight\", \"$keywords\", "$topic");"
	echo "$sql"
	mysql -u "$user" -p"$psd" <<< "$sql"
done < "$file"

mysql -u "$user" -p"$psd" <<EOF
use curation_proj;
show tables;
select * from curation_proj.admincuration_dataset;
EOF
