#!/usr/bin/env bash
# vim: set noexpandtab tabstop=2:

set -v

user=$1
psd=$2
file=$3

mysql -u "$user" -p"$psd" <<EOF
use curation_proj;
show tables;
select b.accNo, d.username, a.result, a.comment, c.topic
from 
  administrator_curation a, 
	administrator_dataset b,
	administrator_topic c,
	auth_user d
where
	a.data_id_id = b.id
	and
	a.user_id_id = d.id
	and
	a.topic_id_id = c.id
EOF

