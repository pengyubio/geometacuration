from django import forms
from admincuration.models import Curation

class CurationFrom(forms.ModelForm):
    class Meta:
        model = Curation
        fields = ['result', 'comment']
        widgets = {
            'result':forms.RadioSelect(attrs={'type': 'radio', 'name':'selector'}),
            'comment': forms.Textarea(attrs={'row': 10,'style':'top: 10px; position: relative','placeholder':'leave your comment'})
        }
       
