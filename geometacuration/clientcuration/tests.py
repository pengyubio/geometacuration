# vim: set noexpandtab tabstop=2 shiftwidth=2 softtabstop=-1 fileencoding=utf-8:

from django.test import TestCase
import os
from django.test.client import RequestFactory
from django.contrib.sessions.middleware import SessionMiddleware
from django.contrib.messages.storage.fallback import FallbackStorage
from .views import *
from django.contrib.auth.models import AnonymousUser, User
from admincuration.models import   Topic, Curation, Dataset
from django.conf import settings

# Create your tests here.
class CuratorTestCase(TestCase):
    def setUp(self):
        if not os.path.exists(settings.TMPLOC):
            os.makedirs(settings.TMPLOC)
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
                username='test', email='lizhao.informatics@gmail.com', password='top_secret')
        self.topic = Topic(topic='test')
        self.topic.save()
        self.dataset = Dataset(
                title = 'Brown and white adipocyte differentiation'
                , accNo = 'GSE7032'
                , pubNo = '17360536'
                , topic = self.topic.id)
        self.dataset.save()
        self.curation = Curation(
                topic_id = self.topic
                , data_id = self.dataset
                , user_id = self.user
                )
        self.curation.save()
    
    def test_save_topic(self):
        topics = Topic.objects.all()
        self.assertEqual(len(topics), 1)

    def test_dnldpmc(self):
        try:
            dnldcleanpmc('PMC5177447')
        except:
            self.assertTrue(False)
    def test_crtreg(self):
        try:
            crtreg('1;2')
            crtreg('1')
            crtreg('')
        except:
            self.assertTrue(False)
    def test_pubmedidconvert(self):
        try:
            pubmedidconvert('29658791')
            pubmedidconvert('')
        except:
            self.assertTrue(False)
    def test_dnldcleangeo(self):
        try:
            dnldcleangeo('acc111')
            dnldcleangeo('GSE100131')
            dnldcleangeo('')
        except:
            self.assertTrue(False)
    def test_curation_list(self):
        request = self.factory.get('/')
        request.user = self.user
        middleware = SessionMiddleware()
        middleware.process_request(request)
        request.session.save()
        l = curation_list(request, self.user.id, -1)
        crt_data_id = [x.data_id_id for x in l]
        print 'length is ' + str(len(crt_data_id))
        for x in l:
            print 'curation id is ' + str(x.data_id_id)
        self.assertEqual(len(l), 1)
    def test_index(self):
        request = self.factory.get('/')
        request.user = self.user
        middleware = SessionMiddleware()
        middleware.process_request(request)
        request.session.save()
        response = index(request, self.user.id)
        self.assertEqual(response.status_code, 200)
    def test_dnldcleanpubmed(self):
        try:
            dnldcleanpubmed('111')
            dnldcleanpubmed(self.dataset.pubNo)
        except:
            self.assertTrue(False)
    def test_pubmed(self):
        request = self.factory.get('/')
        request.user = self.user
        response = pubmed(request, self.user, self.dataset.id, self.curation.id)
        self.assertEqual(response.status_code, 200)
    def test_pmc(self):
        request = self.factory.get('/')
        request.user = self.user
        response = pmc(request, self.user, self.dataset.id, self.curation.id)
        self.assertEqual(response.status_code, 200)
    def test_curation(self):
        request = self.factory.get('/')
        request.user = self.user
        response = pmc(request, self.user, self.dataset.id, self.curation.id)
        self.assertEqual(response.status_code, 200)
 
