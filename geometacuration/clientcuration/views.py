import django
from django.shortcuts import render, redirect
from admincuration.models import   Topic, Curation, Dataset, Summary
from django.template import loader
from django.http import HttpResponse,HttpResponseRedirect
from .forms import CurationFrom
from django.utils import timezone
from datetime import datetime
import requests
from django.contrib.auth.decorators import login_required
from django.db import connection

from django.template.response import TemplateResponse
import random, string
import os
from bs4 import BeautifulSoup
import json
from django.views.decorators.csrf import csrf_exempt
import re
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.conf import settings
TMPLOC = settings.TMPLOC

@login_required(login_url='/')
def index(request,userid):
    close_connection()
    select_topic = request.POST.get('selector',  request.session.get('select_topic',-1))
    request.session['select_topic'] = select_topic
    items_per_page = request.POST.get('items_per_page',  request.session.get('items_per_page',10))
    request.session['items_per_page'] = items_per_page
    nav = request.POST.get('navbar', request.session.get('nav', 0))
    request.session['nav'] = nav
    topic = Topic.objects.all()
    user_topic_set = Curation.objects.values_list('topic_id', flat = True).filter(user_id = userid).distinct()
    curation = curation_list(request, userid, -1)
    crt_data_id = [x.data_id_id for x in curation]
    dataset = Dataset.objects.filter(pk__in = crt_data_id).order_by('accNo')

    page = request.GET.get('page', 1)
    paginator = Paginator(dataset, items_per_page)
    try:
        dataset = paginator.page(page)
    except PageNotAnInteger:
        dataset = paginator.page(1)
    except EmptyPage:
        dataset = paginator.page(paginator.num_pages)

    template = loader.get_template('curator/index.html')
    count = curation.count()
    context = {
        'curation' : curation,
        'dataset' : dataset,
        'select_topic' : int(select_topic),
        'items_per_page' : int(items_per_page),
        'topics': topic,
        'user_id' : userid,
        'nav' : nav,
        "count" : count,
        "user_topic_set" : user_topic_set,
    }
    if request.user.id == int(userid) :
        return HttpResponse(template.render(context, request))
    else:
        return HttpResponseRedirect('/')

def form2curation(curation, form):
    curation.result = form.cleaned_data.get('result')
    curation.comment = form.cleaned_data.get('comment')
    curation.submit = True
    curation.date = timezone.now()
    return curation

@csrf_exempt
@login_required
def docuration(request, userid, dataset_id, curation_id):
    close_connection()
    curation = Curation.objects.get(pk=curation_id)
    form = CurationFrom(request.POST or None)
    if form.is_valid() and request.user.is_authenticated:
        curation = form2curation(curation, form)
        curation.save()
        old_curation_id = curation.pk
        curations = curation_list(request, userid, old_curation_id)

        if curations.count() > 0:
            return redirect("curation", userid = userid, dataset_id = curations[0].data_id_id, curation_id = int(curations[0].pk))
        else:
            return redirect("index", userid = userid)
    else:
        print 'form not valid!!!'
        return HttpResponseRedirect(request.META.get('HTTP_REFERER','/'))

def curation_list(request, userid, old_curation_id):
    select_topic = int(request.session.get('select_topic', -1))
    if select_topic == -1:
        topic_set_tmp = Curation.objects.values_list('topic_id', flat = True).filter(user_id = userid).distinct()
    else:
        topic_set_tmp = [select_topic]
    nav = int(request.session.get('nav', 0))
    submit_flag = nav != 0
    if nav <= 1:
        result = ['Y', 'N']
    else:
        result = ['U']
    return Curation.objects.filter(
            user_id = userid
            , submit = submit_flag
            , topic_id__in = topic_set_tmp
            , result__in = result
            , id__gt = old_curation_id
            )

@csrf_exempt 
@login_required 
def curation(request,userid,dataset_id,curation_id):
    close_connection()
    dataset = Dataset.objects.get(pk = dataset_id)
    topic = Topic.objects.get(
            pk = Curation.objects.values_list('topic_id',flat = True).get(pk = curation_id)
            )
    curation = Curation.objects.get(pk = curation_id)
    msg_geo = highlightpage(
            dnldcleangeo(dataset.accNo)
            , crtreg([topic.highlight, dataset.highlight, dataset.keywords])
            )
    fullpath = os.path.join(TMPLOC, 'tmp/geo_tmp', touchhtml())
    if not os.path.exists(os.path.dirname(fullpath)):
        os.makedirs(os.path.dirname(fullpath))
    file_ = open(fullpath,'w')
    file_.write(msg_geo)
    file_.close()
    request.session['geopage_path'] = fullpath
    convert_json = pubmedidconvert(dataset.pubNo)
    form = CurationFrom(
            initial={
                'comment':curation.comment 
                , 'result':curation.result 
                })
    context = {
            'dataset' : dataset,
            'user_id' : userid,
            'topic' : topic,
            'form' : form,
            'cur_comment' : curation.comment,
            'cur_result' : curation.result,
            'cur_submit' : curation.submit,
            'jsonString' : convert_json,
        }
    template = loader.get_template('curator/curation.html')
    return HttpResponse(template.render(context, request))

def dnldcleangeo(accNo):
    fullpath = os.path.join(TMPLOC, 'cache/geo/', accNo, '.html')
    if os.path.isfile(fullpath):
        file_ = open(fullpath,'r')
        soup_pagecontent_str = file_.read()
        file_.close()
        return soup_pagecontent_str
    cache_flag = True
    try:
        pagecontent = requests.get("https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=" + accNo).content
        pagecontent = pagecontent.replace('src="','src="https://www.ncbi.nlm.nih.gov')
        pagecontent = pagecontent.replace('SRC="','src="https://www.ncbi.nlm.nih.gov')
        pagecontent = pagecontent.replace('href="/geo','href="https://www.ncbi.nlm.nih.gov/geo')
        pagecontent = pagecontent.replace('href="/Taxonomy','href="https://www.ncbi.nlm.nih.gov/Taxonomy')
        pagecontent = pagecontent.replace('href="/pubmed/', 'href="https://www.ncbi.nlm.nih.gov/pubmed/')
        pagecontent = pagecontent.replace('background="/coreweb','background="https://www.ncbi.nlm.nih.gov/coreweb')
        soup_pagecontent = BeautifulSoup(pagecontent,"html5lib")
        [ l.decompose() for l in soup_pagecontent.find_all('li') ]
        [ f.decompose() for f in soup_pagecontent.find_all('form') ]
        soup_pagecontent.find('table',{'width': 740})['width'] = '100%'
        soup_pagecontent.find('table',{'width': 600})['width'] = '100%'
        #[ s.decompose() for s in soup_pagecontent.find_all('script')[-2:] ]
        soup_pagecontent_str=str(soup_pagecontent)
        #soup_pagecontent_str=str(soup_pagecontent.find('table',{'width': 740}).extract())
    except:
        cache_flag = False
        soup_pagecontent_str = '<h2>something wrong, please refresh to try again!</h2>'
    if cache_flag:
        if not os.path.exists(os.path.dirname(fullpath)):
            os.makedirs(os.path.dirname(fullpath))
	file_ = open(fullpath,'w')
        file_.write(soup_pagecontent_str)
        file_.close()
    return soup_pagecontent_str

def ncbi(request,user,dataset_id,curation_id):
    print request.session.get('geopage_path')
    return TemplateResponse(request
            , request.session.get('geopage_path')
            ).render()

def pubmedidconvert(pubmedid):
    convert_url = 'https://www.ncbi.nlm.nih.gov/pmc/utils/idconv/v1.0/?idtype=pmid&format=json&ids='+pubmedid
    try:
        content = requests.get(convert_url).content
    except:
        content = '{}'
    return content

def dnldcleanpubmed(pubmedid):
    fullpath = os.path.join(TMPLOC, 'cache/pubmed/', pubmedid, '.html')
    if os.path.isfile(fullpath):
        file_ = open(fullpath,'r')
        soup_pagecontent_str = file_.read()
        file_.close()
        return soup_pagecontent_str
    cache_flag = True
    try:
        pagecontent = requests.get('https://www.ncbi.nlm.nih.gov/pubmed/'+pubmedid).content
        pagecontent = pagecontent.replace('img src="/pmc','img src="https://www.ncbi.nlm.nih.gov/pmc')
        #pagecontent = pagecontent.replace('href="/','href="https://www.ncbi.nlm.nih.gov/')
        soup_pagecontent = BeautifulSoup(pagecontent,"html5lib")
        #soup_pagecontent_str = str(soup_pagecontent.find('div',{'id': 'maincontent'}).extract())
        #soup_pagecontent.find('div',{'id': 'maincontent'})['class'].remove('nine_col')
        soup_pagecontent.find('div',{'id': 'disc_col'}).decompose()
        #soup_pagecontent.find('div',{'class': 'supplemental col three_col last'}).decompose()
        soup_pagecontent.find('div',{'id': 'footer'}).decompose()
        soup_pagecontent.find('div',{'id': 'universal_header'}).decompose()
        soup_pagecontent.find('div',{'class': 'header'}).decompose()
        #[ s.decompose() for s in soup_pagecontent.find_all('script') ]
        soup_pagecontent_str = str(soup_pagecontent)
    except:
        cache_flag = False
        soup_pagecontent_str = '<h2>something wrong, please refresh to try again!</h2>'
    if cache_flag:
        if not os.path.exists(os.path.dirname(fullpath)):
            os.makedirs(os.path.dirname(fullpath))
        file_ = open(fullpath,'w')
        file_.write(soup_pagecontent_str)
        file_.close()
    return soup_pagecontent_str

from tempfile import mktemp
def touchhtml():
    return mktemp(
            dir = './'
            , prefix = datetime.now().strftime("%Y/%m/%d/%H_%M_%S_%f")
            , suffix = '.html'
            )

def pubmed(request,user,dataset_id,curation_id):
    close_connection()
    pubmedid = Dataset.objects.values_list('pubNo',flat = True).get(pk = dataset_id)
    topic = Topic.objects.get(
            pk = Curation.objects.values_list('topic_id',flat = True).get(pk=curation_id)
            )
    dataset = Dataset.objects.get(pk = dataset_id)
    msg_pmed = highlightpage(
            dnldcleanpubmed(pubmedid)
            , crtreg([topic.highlight, dataset.highlight, dataset.keywords])
            )
    #PY
    fullpath = os.path.join(TMPLOC, 'tmp/pubmed_tmp/', touchhtml())
    if not os.path.exists(os.path.dirname(fullpath)):
        os.makedirs(os.path.dirname(fullpath))
    f = open(fullpath, 'w')
    f.write(msg_pmed)
    f.close()
    return TemplateResponse(request, fullpath).render()

def crtreg(highlights):
    highlight = [x.encode('utf-8') if x is not None else '' for x in highlights]
    soup_pattern = [re.split(';', x) for x in highlight]
    highlight_keywords = [['(\\b'+str(y)+'(s?)'+'\\b)|' if len(y) >= 3 else '' for y in x] for x in soup_pattern]
    regex = [re.compile(''.join(x)+'\b', re.IGNORECASE) for x in highlight_keywords]
    return regex

def highlightpage(content, regexs):
    colors=['#ffb7b7', '#a8d1ff', '#fff2a8']
    for i in range(len(regexs)):
        j = 0
        output = ''
        for m in regexs[i].finditer(content):
            output += "".join([content[j:m.start()],
                "<mark style='background-color:" + colors[i]  + ";'>",
                content[m.start():m.end()],
                "</mark>"])
            j = m.end()
        content = "".join([output, content[j:]])
    return content

def pmc(request,user,dataset_id,curation_id):
    close_connection()
    pubmedid = Dataset.objects.values_list('pubNo',flat = True).get(pk = dataset_id)
    jsonString = pubmedidconvert(pubmedid)
    j = json.loads(jsonString) 
    topic_id = Curation.objects.values_list('topic_id',flat = True).get(pk=curation_id)
    topic = Topic.objects.get(pk = topic_id)
    dataset = Dataset.objects.get(pk = dataset_id)
    pmc_delay=''
    fullpath = os.path.join(TMPLOC, 'tmp/pmc_tmp/', touchhtml())
    if not os.path.exists(os.path.dirname(fullpath)):
        os.makedirs(os.path.dirname(fullpath))
    if j.get("records") != None:
        if j["records"][0].get("pmcid") != None:
            pmcid = j["records"][0]["pmcid"]
            msg_pmc = dnldcleanpmc(pmcid)
            pmc_delay = msg_pmc.find("This article has a delayed release")
            regexs = crtreg([topic.highlight, dataset.highlight, dataset.keywords])
            msg_pmc = highlightpage(msg_pmc, regexs)
            cov = open(fullpath, 'w')
            cov.write(msg_pmc)
            if not cov.closed:
                cov.close()
            
    context = {
                'pmc_delay': pmc_delay,
            }
    return TemplateResponse(request, fullpath, context).render()

from selenium import webdriver
def dnldcleanpmc(pmcid):
    fullpath = os.path.join(TMPLOC, 'cache/pmc/', pmcid + '.html')
    print fullpath
    cache_flag = True
    if os.path.isfile(fullpath):
        file_ = open(fullpath,'r')
        soup_pagecontent_str = file_.read()
        file_.close()
        return soup_pagecontent_str
    url_pmc = 'https://www.ncbi.nlm.nih.gov/pmc/articles/'+pmcid
    try:
        print url_pmc
        msg_pmc_obj = webdriver.PhantomJS(service_args=['--ignore-ssl-errors=true', '--ssl-protocol=TLSv1'])
        msg_pmc_obj.get(url_pmc)
        msg_pmc = msg_pmc_obj.page_source.encode('utf-8')
        msg_pmc_obj.quit()
        msg_pmc = msg_pmc.replace('img src="/pmc','img src="https://www.ncbi.nlm.nih.gov/pmc')
        msg_pmc = msg_pmc.replace('href="/pmc','href="https://www.ncbi.nlm.nih.gov/pmc')
        msg_pmc = msg_pmc.replace('src-large="/pmc','src-large="https://www.ncbi.nlm.nih.gov/pmc')
        soup_pagecontent = BeautifulSoup(msg_pmc, "html5lib")
        soup_pagecontent.find('div',{'id': 'rightcolumn'}).decompose()
        soup_pagecontent.find('div',{'class': 'top'}).decompose()
        soup_pagecontent.find('div',{'id': 'footer'}).decompose()
        soup_pagecontent.find('div',{'id': 'maincontent'})['class'].remove('eight_col')
        #[ s.decompose() for s in soup_pagecontent.find_all('script') ]
        soup_pagecontent_str = str(soup_pagecontent)
    except Exception, e:
        cache_flag = False
        soup_pagecontent_str = '<h2>something wrong, please refresh to try again!</h2>'
    if cache_flag:
        if not os.path.exists(os.path.dirname(fullpath)):
            os.makedirs(os.path.dirname(fullpath))
        file_ = open(fullpath,'w')
        file_.write(soup_pagecontent_str)
        file_.close()
    return soup_pagecontent_str

def close_connection():
    if not connection.in_atomic_block:
        connection.close()
