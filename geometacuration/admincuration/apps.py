from django.apps import AppConfig


class AdmincurationConfig(AppConfig):
    name = 'admincuration'
